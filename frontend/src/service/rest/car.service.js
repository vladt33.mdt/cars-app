import { BaseService } from "./base.service";

class CarService extends BaseService {
  async getCars() {
    return await this.service.get("/cars");
  }
}


export default new CarService();