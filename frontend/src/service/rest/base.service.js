import axios from "axios";

export class BaseService {
  constructor() {
    this.baseUrl = `${process.env.REACT_APP_BE_HOST || 'http://localhost:3000'}`;
    this.service = axios.create({
      baseURL: this.baseUrl,
    });
    this.service.interceptors.response.use(
      this.handleSuccess,
      this.handleError
    );
  }

  handleSuccess(response) {
    return response;
  }

  handleError = (error) => {
    console.error({error});
    return Promise.reject(error);
  };
}
