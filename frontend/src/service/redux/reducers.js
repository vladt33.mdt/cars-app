import { combineReducers } from "redux";
import carReducer from "./car/reducer";

export default combineReducers({
  cars: carReducer,
});
