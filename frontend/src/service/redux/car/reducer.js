import { ADD_CARS, STORE_ERROR } from "../constants/action-types";

const initialState = {
  cars: {
    data: [],
    items: 0,
    message: "ready",
  },
};

const carReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_CARS:
      return action.payload;
    case STORE_ERROR:
    default:
      return state;
  }
};

export default carReducer;
