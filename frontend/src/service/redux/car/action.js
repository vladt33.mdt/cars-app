// src/js/actions/index.js
import carService from "../../rest/car.service";
import { ADD_CARS, STORE_ERROR } from "../constants/action-types";

export const setCars = () => async (dispatch) => {
  try {
    const response = await carService.getCars();
    dispatch({
      type: ADD_CARS,
      payload: response?.data ? response.data : {},
    });
  } catch (e) {
    dispatch({
      type: STORE_ERROR,
      payload: { message: `${e}` },
    });
  }
};
