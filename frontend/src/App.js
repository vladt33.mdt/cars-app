import "./App.css";
import CarList from "./components/car-list/car-list.component";

function App() {
  return (
    <div className="App">
      <CarList></CarList>
    </div>
  );
}

export default App;
