import React, { Component } from "react";
import { connect } from "react-redux";
import { setCars } from "../../service/redux/car/action";
import CarListItem from "./car-models-list/car-model-list.component";

class CarList extends Component {
  componentDidMount() {
    this.props.setCars().then(() => console.log({ props: this.props }));
  }

  render = () => {
    return (
      <React.Fragment>
        {this.props.cars?.data?.map((res) => {
          return (
            <CarListItem
              key={res._id}
              brandId={res._id}
              modelItem={res.models}
            ></CarListItem>
          );
        }) || []}
      </React.Fragment>
    );
  };
}

export default connect(
  (state) => ({
    cars: state?.cars || { data: [], items: 0, message: "ready" },
  }),
  { setCars }
)(CarList);
