import "./car-item.component.css";

export const CarItem = (prop) => {
  const { carItem } = prop;
  return (
    <div className="card_container">
      <div className="card">
        <img
          src={`${process.env.REACT_APP_BE_HOST || "http://localhost:3000"}/${
            carItem.model.modelImage
          }`}
          alt={`not found ${carItem.model.modelName}`}
        />
      </div>
      <div className="container card__description">
        <h4>
          <b>{carItem.model.modelName}</b>
        </h4>
        {/* <p>{carItem.description}</p> */}
      </div>
    </div>
  );
};
