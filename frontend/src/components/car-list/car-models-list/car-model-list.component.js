import React from "react";
import "./car-model-list.component.css";
import { CarItem } from "./car-item/car-item.component";

export default class CarModelList extends React.Component {
  constructor(props) {
    super(props);
    this.brandId = this.props.brandId;
    this.modelItem = this.props.modelItem;
  }
  render = () => {
    return (
      <div className="item-container" style={{ display: "flex" }}>
        <div className="item-container__header">
          <img
            src={`https://raw.githubusercontent.com/filippofilip95/car-logos-dataset/master/logos/original/${this.brandId}.png`}
            onError={(evt) => {
              if (!this.urlChecked) {
                evt.target.src = `https://raw.githubusercontent.com/filippofilip95/car-logos-dataset/master/logos/original/${this.brandId}.jpg`;
                this.urlChecked = true;
              }
              evt.target.onerror = null;
            }}
            alt="logo non trovato"
          />
          <h2 className="item-container__model_title">
            {this.brandId?.toUpperCase().replace("-", " ")}
          </h2>
          <div className="item-container__header__action">
            <label>{"Vedi Tutto >"}</label>
          </div>
        </div>
        <div style={{ display: "flex" }}>
          {this.modelItem.map((modelItem) => (
            <CarItem
              key={modelItem.model.modelId.toString()}
              carItem={modelItem}
            ></CarItem>
          ))}
        </div>
      </div>
    );
  };
}
