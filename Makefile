BUILD_PATH='./ci'

.PHONY: help

# sed [OPZIONI]... {file regole filtraggio} [file di input]
help:
	@sed -ne '/@sed/!s/## //p' $(MAKEFILE_LIST)

up: ## create and starts the containers in the background and leaves them running
	cd ${BUILD_PATH} && docker-compose -p cars_app up --build -d
#   docker-compose -p mrc up --build --remove-orphans

start: ## create and starts the containers in the background and leaves them running
	cd ${BUILD_PATH} && \
	docker-compose -p cars_app start

up-mongo:
	cd ${BUILD_PATH} && docker-compose -p cars_app up --build -d mongo
	
stop: ## stop services
	cd ${BUILD_PATH} && \
	docker-compose -p cars_app stop

down: ## stop and remove containers, networks, images, and volumes
	cd ${BUILD_PATH} && \
	docker-compose -p cars_app down -v
