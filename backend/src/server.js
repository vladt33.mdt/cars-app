const app = require('./app');
const http = require('http');
const { AppConfig: config } = require('./services/app.config');
const { mongoConnect } = require('./mongo/mongo_connection');
const { exit } = require('process');

const carRepository = require('./mongo/repositories/car.repository');

/**
 * Get port from environment and store in Express.
 */
const init = async () => {
  let port = config.PORT || 3000;
  app.set('port', port);

  /**
   * Create HTTP server.
   */

  var server = http.createServer(app);

  /**
   * Listen on provided port, on all network interfaces.
   */
  server.on('error', onError);
  server.on('listening', () => onListening(server));

  try {
    await mongoConnect();
    await carRepository.migrateDb();
    server.listen(port);
  } catch (e) {
    console.log(`error: ${e}`);
    exit(1);
  }

  return server;
};

/**
 * Event listener for HTTP server "error" event.
 */

const onError = (error) => {
  if (error.syscall !== 'listen') {
    throw error;
  }
  const port = config.port || 3000;

  var bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
    default:
      throw error;
  }
};

/**
 * Event listener for HTTP server "listening" event.
 */

const onListening = (server) => {
  var addr = server.address();
  var bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
  console.log('Listening on ' + bind);
};

init().then((res) => console.log('app is running'));
