const dotenv = require('dotenv').config();

exports.AppConfig = {
  PORT: process.env.PORT || 3000,
  CARS_DATA_URL: process.env.CARS_DATA_URL,
  MONGO_USER: process.env.MONGO_USER || 'admin',
  MONGO_PWD: process.env.MONGO_PWD || 'rootroot',
  MONGO_HOST: process.env.MONGO_HOST || 'localhost',
  MONGO_PORT: process.env.MONGO_PORT || '27017',
};
