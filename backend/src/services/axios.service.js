var axios = require('axios');

var {AppConfig:config} = require('./app.config');

exports.getData = async () => {
  return await axios({ url: config.CARS_DATA_URL, method: 'get' });
};
