var express = require('express');
var router = express.Router();
const carRepository = require('../mongo/repositories/car.repository');

/* GET users listing. */
router.get('/', async (req, res, next) => {
  try {
    const cars = await carRepository.findAllGroupedByBrandAndModel(5);
    if (cars && cars.length > 0) {
      res.status(200).json({
        data: cars,
        items: cars.length,
        message: 'done',
        status: res.status,
      });
    } else {
      res.status(404).send({
        message: 'no cars found',
        status: res.status,
      });
    }
  } catch (e) {
    res.status(500).json({
      message: `an error has occurred: ${e}`,
      status: 500,
    });
  }
});

module.exports = router;
