const { MigrationSchema } = require('../schemas/migration.schema');
const mongoose = require('mongoose');

class MigrationRepository {
  get model() {
    return this._model;
  }

  constructor() {
    this._model = mongoose.model('MigrationModel', MigrationSchema);
  }

  async create(migration_data) {
    try {
      return await this._model.create(migration_data);
    } catch (e) {
      console.log(`an error has occurred: ${e}`);
    }
  }

  async findOneByDate() {
    try {
      return await this._model.findOne({
        date: {
          $lte: new Date().getTime(),
        },
        status: 'DONE',
      }).lean();
    } catch (e) {
      console.error(`an error is occurred: ${e}`);
      throw e;
    }
  }
}

module.exports = new MigrationRepository();
