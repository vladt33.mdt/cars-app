const { CarModel } = require('../models/car.model');
const migrationRepository = require('./migration.repository');
const { getData } = require('../../services/axios.service');
class CarRepository {
  get model() {
    return this._model;
  }

  constructor() {
    this._model = CarModel;
  }

  async createCar(carModel) {
    try {
      return await this._model.create({ ...carModel });
    } catch (e) {
      console.log(`an error has occurred: ${e}`);
    }
  }

  async insertBatch(cars, chunkSize) {
    let res = [];
    try {
      for await (let [value, idx, chunkNum] of this.createAsyncChunks(cars, chunkSize)) {
        console.log(`create cars from ${idx} to ${idx + value.length} of ${cars.length}`);
        res = [...res, ...value];
      }
    } catch (e) {
      console.log(`an error has occurred: ${e}`);
    }
    return res;
  }

  async findAll() {
    return (await this._model.find().lean()) || [];
  }

  /**
   *
   *  recupera la lista delle macchine divise per brand
   *
   * @param {Number} limit
   * @return {Array<CarModel>}
   * @memberof CarRepository
   * @deprecated
   */
  async findAllByBrandId(limit) {
    try {
      return await this._model
        .aggregate()
        .group({
          _id: '$brandId',
          cars: {
            $push: {
              _id: '$$ROOT._id',
              title: '$$ROOT.title',
              description: '$$ROOT.description',
              modelId: '$$ROOT.modelId',
              imageUrl: '$$ROOT.imageUrl',
            },
          },
          count: { $sum: 1 },
        })
        .project({
          _id: '$_id',
          cars:
            limit && limit > 0
              ? {
                  $slice: ['$cars', 5],
                }
              : '$cars',
          total: '$count',
        })
        .sort({
          _id: 1,
        });
    } catch (e) {
      console.error(`unable to get cars: ${e}`);
      throw e;
    }
  }

  async findAllGroupedByBrandAndModel(limit) {
    let dataPromise = this._model
      .aggregate()
      .group({
        _id: {
          brand: {
            brandId: '$brandId',
            brandName: '$brand',
          },
          model: {
            modelId: '$modelId',
            modelName: '$model',
            imageUrl: '$imageUrl',
          },
        },
        models: {
          $push: '$$ROOT',
        },
      })
      .sort({
        '_id.brand.brandId': 1,
      })
      .group({
        _id: '$_id.brand.brandId',
        models: {
          $push: {
            model: {
              modelId: '$_id.model.modelId',
              modelName: '$_id.model.modelName',
              modelImage: '$_id.model.imageUrl',
            },
            cars: {
              $map: {
                input: '$models',
                as: 'mod',
                in: {
                  title: '$$mod.title',
                  description: '$$mod.description',
                  category: '$$mod.category',
                  availability: '$$mod.availability',
                  promo: '$$mod.promo',
                },
              },
            },
          },
        },
      });
    if (limit) {
      dataPromise = dataPromise.project({
        _id: '$_id',
        models: {
          $slice: ['$models', limit],
        },
      });
    }
    dataPromise = dataPromise.sort({
      _id: 1,
      'models.model.modelId': 1,
    });
    return (await dataPromise) || [];
  }

  async migrateDb() {
    const migration = await migrationRepository.findOneByDate();
    const imageTmpData = {};
    if (!migration) {
      const {
        status,
        data: { data },
      } = await getData();
      if (status === 200) {
        data.forEach((dt) => {
          imageTmpData[dt.modelId] = imageTmpData[dt.modelId] || `images/auto_${Math.floor(Math.random() * 2) + 1}.jpg`;
          dt.imageUrl = imageTmpData[dt.modelId];
        });
        const res = await this.insertBatch(data, 100);
        await migrationRepository.create({
          miration_date: new Date().getTime(),
          item_saved: res.length,
          collection_name: 'car',
          status: 'DONE',
        });
      }
    }
  }

  async *createAsyncChunks(data, chunkSize) {
    if (data.constructor === [].constructor) {
      for (let i = 0, j = 0; i < data.length; i += chunkSize, j++) {
        const dt = data.slice(i, i + chunkSize).map((res) => {
          if (res._id) {
            const { _id, ...other } = res;
            return other;
          }
          return res;
        });

        yield [await this._model.insertMany(dt), i, j];
      }
    }
  }
}
module.exports = new CarRepository();
