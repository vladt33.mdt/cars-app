const mongoose = require('mongoose');
const { CarSchema } = require('../schemas/car.schema');
var CarModel = mongoose.model('CarModel', CarSchema);
module.exports = { CarModel };
