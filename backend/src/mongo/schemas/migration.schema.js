const Schema = require('mongoose').Schema;

module.exports.MigrationSchema = new Schema(
  {
    miration_date: Number,
    item_saved: Number,
    collection_name: String,
    status: String,
  },
  { collection: 'migration' }
);
