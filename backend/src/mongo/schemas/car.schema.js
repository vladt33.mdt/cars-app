const Schema = require('mongoose').Schema;

const CarSchema = new Schema(
  {
    title: String,
    description: String,
    model: String,
    modelId: String,
    modelDetailId: String,
    modelDetail: String,
    brand: String,
    brandId: String,
    category: String,
    availability: String,
    imageUrl: String,
    promo: Boolean,
  },
  { collection: 'car' }
);

module.exports = { CarSchema };
