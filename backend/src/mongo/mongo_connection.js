//Import the mongoose module
var mongoose = require('mongoose');
var { AppConfig: config } = require('../services/app.config');

//Set up default mongoose connection
//Define a schema
const mongoConnect = async () => {
  var mongoDB = `mongodb://${config.MONGO_USER}:${config.MONGO_PWD}@${config.MONGO_HOST}:${config.MONGO_PORT}/cars?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&directConnection=true&ssl=false`;
  if (
    !mongoose.connection ||
    !mongoose.connection ||
    mongoose.connection?.readyState !== mongoose.connection.states.connected
  ) {
    try {
      connection = mongoose.connection;

      connection.once('open', () => console.log('MongoDB connection opened!'));

      connection.on('error', console.error.bind(console, 'MongoDB connection error:'));
      connection.on('connecting', () => console.log('connecting to MongoDB...'));
      connection.on('connected', () => console.log('MongoDB connected!'));
      connection.on('reconnected', () => console.log('MongoDB reconnected!'));
      connection.on('disconnected', () => console.log('MongoDB disconnected!'));

      await mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });
    } catch (e) {
      console.error(e);
      throw e;
    }
  }
  return connection;
};

module.exports = {
  mongoConnect,
};
