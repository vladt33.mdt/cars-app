# Cars App

## Descrizione

Applicazione gestionale di un concessionario

## Struttura

il progetto è strutturato come un monorepo contenente:

- backend in NodeJs + Express `/backend`
- frontend in React + Redux `/frontend`
- docker per avviare l'applicazione `/ci`
- setting vscode per avviare il backend in debug `/.vscode`

## Primo Avvio Applicazione

- clonare il repository
- spostarsi nella root del progetto
- eseguire comando
  `make up`
  - se non si ha `make` installato, portarsi nella cartella ci
    `cd ./ci`
  - ed eseguire il comando
    `docker-compose -p cars_app up --build -d`
- andare al seguente url
  [http://localhost:4200](http://localhost:4200)

## Successivi avvii

- spostarsi nella root del progetto
- eseguire comando
  `make start`
  - se non si ha `make` installato, portarsi nella cartella ci
    `cd ./ci`
  - ed eseguire il comando
    `docker-compose -p cars_app start`
- andare al seguente url
  [http://localhost:4200](http://localhost:4200)


## Avvio in locale

- avviare docker mongo in locale
  - nella root di progetto inviare il comando `make up-mongo`
  - altrimenti 
    - spostarsi nella cartella ci e inviare il comando `docker-compose -p cars_app up --build -d mongo`
    - avviare il progetto backend 
    - `cd backend`
    - `npm run start`
      - oppure `npm run debug` per utilizzare nodemon
    - avviare il progetto frontend 
      - `cd frontend`
      - `npm run start`

## .evn file

- per avviare il progetto in locale i progetti necessitano di variabili d'ambiente in file `.env`
  - per il frontend, bisogna creare il file `frontend/.env`
  - per il backend, bisogna creare il file `backend/.env`
- esempi di variabili da inserire nel progetto si possono trovare nei file 
  - `ci/.env.fe.local` per il frontend
  - `ci/.env.be.local` per il backend
